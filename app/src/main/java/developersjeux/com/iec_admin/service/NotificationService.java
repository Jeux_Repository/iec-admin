package developersjeux.com.iec_admin.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.Random;

import developersjeux.com.iec_admin.R;
import developersjeux.com.iec_admin.activity.ChatActivity;
import developersjeux.com.iec_admin.model.UsersModel;
import developersjeux.com.iec_admin.universal.APIs;
import developersjeux.com.iec_admin.universal.AppController;
import developersjeux.com.iec_admin.universal.HelperClass;
import developersjeux.com.iec_admin.universal.IecPreferences;

public class NotificationService extends Service {

    private IecPreferences preferences;
    private int counter;
    private String TAG = "VolleySingleUserData";
    private UsersModel model;

    public NotificationService() {

    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        preferences = new IecPreferences(getApplicationContext());
        getNotifications();
    }

    private void getNotifications() {
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                APIs.GET_NOTIFICATION + preferences.getUserId() + APIs.API_KEY, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.v("GetNotification", "Response: " + response.toString());
                        if (response.length() > 0) {
                            Iterator<String> iterator = response.keys();
                            while (iterator.hasNext()) {
                                Random rand = new Random();
                                int i1 = rand.nextInt(100);
                                int i2 = rand.nextInt(100);
                                try {
                                    String id = iterator.next();
                                    JSONObject object = response.getJSONObject(id);
                                    Log.v("InnerObjectSize", object.length() + "");
                                    counter = object.length();
                                    Iterator<String> innerIterator = object.keys();
                                    while (innerIterator.hasNext()) {
                                        String innerId = innerIterator.next();
                                        JSONObject innerObject = object.getJSONObject(innerId);
                                        String message = innerObject.getString("message");
                                        String time = innerObject.getString("time");
                                        String senderId = innerObject.getString("senderId");
                                        String senderName = innerObject.getString("senderName");
                                        String senderImage = innerObject.getString("userImage");

                                        UsersModel model = new UsersModel(senderId, senderImage, senderName, "");

                                        setNotification(senderName, message, i1 + i2, model);
                                        deleteNotification(preferences.getUserId(), senderId, innerId);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                getNotifications();
                            }
                        }, 1500);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                Log.v("GetNotification", "Error: " + error.getMessage());
                getNotifications();
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjReq, "asdsa");
    }

    private void setNotification(String title, String message, int time, UsersModel model) {
        if (message.startsWith("http")) {
            message = "1 file received";
        }

        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra("DATA", model);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, time, intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);
        /*sound type give */
        Uri notification_sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notification_builder = new NotificationCompat.Builder(this, String.valueOf(HelperClass.getTime()));

        notification_builder.setSmallIcon(R.drawable.ic_logo);
        notification_builder.setContentTitle(title);
        notification_builder.setContentText(message);
        notification_builder.setAutoCancel(true);
        notification_builder.setSound(notification_sound);
        notification_builder.setContentIntent(pendingIntent);
        notification_builder.setChannelId(String.valueOf(HelperClass.getTime()));
        notification_builder.setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(time, notification_builder.build());
    }

    private void deleteNotification(final String receiverId, final String senderId, final String notificationId) {
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest dr = new StringRequest(Request.Method.DELETE,
                APIs.DELETE_SINGLE_NOTIFICATION + receiverId + "/" + senderId + "/" + notificationId + APIs.API_KEY,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        deleteNotification(receiverId, senderId, notificationId);
                    }
                }
        );
        queue.add(dr);
    }

    private void getUserData(final String id) {
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                APIs.USERS + id + APIs.API_KEY, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.v("VolleySingleUserData", "Response: " + response.toString());
                        if (response.length() > 0) {
                            try {
                                String Image = response.getString("Image");
                                String displayName = response.getString("displayName");
                                String email = response.getString("email");
                                model.setId(id);
                                model.setImage(Image);
                                model.setName(displayName);
                                model.setEmail(email);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Log.v("VolleySingleUserData", "Error: " + error.getMessage());
            }
        });

        AppController.getInstance().addToRequestQueue(jsonObjReq, TAG);
    }

}
