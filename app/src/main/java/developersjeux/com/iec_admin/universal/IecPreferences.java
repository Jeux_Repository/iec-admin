package developersjeux.com.iec_admin.universal;

import android.content.Context;
import android.content.SharedPreferences;

public class IecPreferences {
    private static String PREF_NAME = "login_pref";
    private String KEY_IS_LOGIN = "is_login";
    private String KEY_USER_ID = "user_id";
    private String KEY_IMAGE = "image";
    private String KEY_API_KEY = "api_key";
    private String KEY_DISPLAY_NAME = "display_name";
    private String KEY_EMAIL = "email";
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    public IecPreferences(Context context) {
        preferences = context.getSharedPreferences(PREF_NAME, 0);
        editor = preferences.edit();
    }

    public void setUserData(boolean value, String id, String image, String apiKey, String displayName, String email) {
        editor.putBoolean(KEY_IS_LOGIN, value);
        editor.putString(KEY_USER_ID, id);
        editor.putString(KEY_IMAGE, image);
        editor.putString(KEY_API_KEY, apiKey);
        editor.putString(KEY_DISPLAY_NAME, displayName);
        editor.putString(KEY_EMAIL, email);
        editor.commit();
    }


    public void setLogin(boolean value) {
        editor.putBoolean(KEY_IS_LOGIN, value);
        editor.commit();
    }

    public boolean isLogin() {
        return preferences.getBoolean(KEY_IS_LOGIN, false);
    }

    public void setUserId(String id) {
        editor.putString(KEY_USER_ID, id);
        editor.commit();
    }

    public String getUserId() {
        return preferences.getString(KEY_USER_ID, "");
    }

    public String getUserImage() {
        return preferences.getString(KEY_IMAGE, "");
    }

    public String getName() {
        return preferences.getString(KEY_DISPLAY_NAME, "");
    }
}
