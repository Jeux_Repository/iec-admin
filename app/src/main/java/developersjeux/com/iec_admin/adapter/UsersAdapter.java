package developersjeux.com.iec_admin.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import developersjeux.com.iec_admin.R;
import developersjeux.com.iec_admin.activity.ChatActivity;
import developersjeux.com.iec_admin.model.UsersModel;

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.UsersViewHolder> implements Filterable {

    ArrayList<UsersModel> list;
    ArrayList<UsersModel> usersListFiltered;
    Context context;
    int size;


    public UsersAdapter(ArrayList<UsersModel> list, Context context, int size) {
        this.list = list;
        this.usersListFiltered = list;
        this.context = context;
        this.size = size;
    }

    @NonNull
    @Override
    public UsersAdapter.UsersViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_view_users, viewGroup, false);
        return new UsersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final UsersAdapter.UsersViewHolder holder, int position) {

        final UsersModel model = usersListFiltered.get(position);

        holder.tv_Name.setText(model.getName());
        Glide.with(context).load(model.getImage()).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {

                holder.civ_Picture.setImageResource(R.drawable.ic_person_black_24dp);
                holder.avLoading.setVisibility(View.GONE);
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                holder.avLoading.setVisibility(View.GONE);
                return false;
            }
        }).into(holder.civ_Picture);

        holder.lnr_Parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, ChatActivity.class)
                        .putExtra("DATA", model)
                );
            }
        });
    }

    @Override
    public int getItemCount() {
        return usersListFiltered.size();
    }

    public class UsersViewHolder extends RecyclerView.ViewHolder {

        CircleImageView civ_Picture;
        TextView tv_Name;
        ProgressBar avLoading;
        LinearLayout lnr_Parent;
        private View view_Online;

        public UsersViewHolder(@NonNull View itemView) {
            super(itemView);
            avLoading = itemView.findViewById(R.id.avLoading);
            civ_Picture = itemView.findViewById(R.id.civ_Picture);
            tv_Name = itemView.findViewById(R.id.tv_Name);
            lnr_Parent = itemView.findViewById(R.id.lnr_Parent);
            view_Online = itemView.findViewById(R.id.view_Online);
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    usersListFiltered = list;
                } else {
                    ArrayList<UsersModel> filteredList = new ArrayList<>();
                    for (UsersModel row : list) {
                        Log.v("onQueryText", "CharString: " + charString);
                        Log.v("onQueryText", "CharSequence: " + charString);
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getName().toLowerCase().contains(charString.toLowerCase())) {
                            //Log.v("onQueryText", "title: : " + row.getTitlte());
                            filteredList.add(row);
                        }
                    }
                    usersListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = usersListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                Log.v("onQueryText", "publishResult: " + "CharSeq: " + charSequence + "    Filter Result: " + filterResults);
                usersListFiltered = (ArrayList<UsersModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

}
