package developersjeux.com.iec_admin.model;

public class Message {
    private String message, sender_id, name;

    public Message() {
    }

    public Message(String message, String sender_id, String name) {
        this.message = message;
        this.sender_id = sender_id;
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getName() {
        return name;
    }

    public String getSender_id() {
        return sender_id;
    }

    public void setSender_id(String sender_id) {
        this.sender_id = sender_id;
    }

    public void setName(String name) {
        this.name = name;
    }
}
