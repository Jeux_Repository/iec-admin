package developersjeux.com.iec_admin.model;

public class ChatModel {
    private String messageId, message, receiverName, time, senderName, senderId;
    private long timeInMils;

    public ChatModel(String message, String receiverName, String time, String senderId) {
        this.message = message;
        this.receiverName = receiverName;
        this.time = time;
        this.senderId = senderId;
    }

    public long getTimeInMils() {
        return timeInMils;
    }

    public void setTimeInMils(long timeInMils) {
        this.timeInMils = timeInMils;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }
}
