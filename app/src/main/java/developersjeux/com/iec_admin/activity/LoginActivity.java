package developersjeux.com.iec_admin.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import developersjeux.com.iec_admin.R;
import developersjeux.com.iec_admin.universal.APIs;
import developersjeux.com.iec_admin.universal.AppController;
import developersjeux.com.iec_admin.universal.IecPreferences;

public class LoginActivity extends AppCompatActivity {

    private EditText et_Email, et_Password;
    private Context context;
    private String TAG = "VOLLEY_LIBRARY_LOGIN";
    private IecPreferences preferences;
    private ProgressBar progressBar;
    private Button btn_SignIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        preferences = new IecPreferences(LoginActivity.this);
        context = LoginActivity.this;
        et_Email = findViewById(R.id.et_Email);
        et_Password = findViewById(R.id.et_Password);
        progressBar = findViewById(R.id.progressBar);
        btn_SignIn = findViewById(R.id.btn_SignIn);
        disableProgress();
    }

    public void signIn(View view) {
        if (!et_Email.getText().toString().equals("")
                && !et_Password.getText().toString().equals("")
                && et_Email.getText().toString().equals("admin")
                && et_Password.getText().toString().equals("admin")) {
            loginWithEmailAndPassword(getString(R.string.admin_login), getString(R.string.admin_pswd));
        } else {
            Toast.makeText(context, "Please enter valid Email and Password", Toast.LENGTH_SHORT).show();
        }
    }

    private void loginWithEmailAndPassword(final String email, final String password) {

        enableProgress();

        Log.v("CheckLogin___", "Email: " + email + "  Password: " + password);
        Log.v("CheckLogin___", "LOGIN API:  " + APIs.LOGIN);
        StringRequest stringRequest = new StringRequest(

                Request.Method.POST,
                APIs.LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.v("CheckLogin___", "Response: " + response);
                        try {
                            JSONObject object = new JSONObject(response);
                            if (object.has("message")) {
                                Log.v("CheckLogin___", "Response Message " + object.toString());
                                Toast.makeText(context, "Invalid Email or Password", Toast.LENGTH_SHORT).show();
                                disableProgress();
                            } else if (object.has("uid")) {
                                try {
                                    Log.v("CheckLogin___", "Response UID " + object.getString("uid"));
                                    loginWithId(object.getString("uid"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            disableProgress();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d("volley", "Error: " + error.getMessage());
                        error.printStackTrace();
                        Log.v("CheckLogin___", "Error: " + error.getMessage());
                        Toast.makeText(context, "Invalid Email or Password", Toast.LENGTH_SHORT).show();
                        disableProgress();
                    }
                }) {

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                params.put("password", password);
                return params;
            }

        };

        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private void loginWithId(final String uid) {
        Log.v("CheckLogin___", "Login with UID" + APIs.USERS + uid + APIs.API_KEY);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                APIs.USERS + uid + APIs.API_KEY
                , null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        Log.v("CheckLogin___", "Response: " + response.toString());
                        try {
                            String Image = response.getString("Image");
                            String api_key = response.getString("api_key");
                            String displayName = response.getString("displayName");
                            String email = response.getString("email");
                            preferences.setUserData(true, uid, Image, api_key, displayName, email);
                            Toast.makeText(context, "Successfully Logged In", Toast.LENGTH_SHORT).show();
                            launchMainActivity();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            disableProgress();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Log.v("CheckLogin___", "Error: " + error.getMessage());
                disableProgress();
                Toast.makeText(context, "Error, Please try again later", Toast.LENGTH_SHORT).show();
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjReq, TAG);
    }

    private void launchMainActivity() {
        startActivity(new Intent(this, MainActivity.class));
        Toast.makeText(context, "Successfully Logged in", Toast.LENGTH_SHORT).show();
        finish();
    }

    private void enableProgress() {
        progressBar.setVisibility(View.VISIBLE);
        btn_SignIn.setEnabled(false);
        btn_SignIn.setAlpha(0.3f);
    }

    private void disableProgress() {
        progressBar.setVisibility(View.GONE);
        btn_SignIn.setEnabled(true);
        btn_SignIn.setAlpha(1);
    }
}
