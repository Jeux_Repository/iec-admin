package developersjeux.com.iec_admin.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import developersjeux.com.iec_admin.R;
import developersjeux.com.iec_admin.adapter.ChatAdapter;
import developersjeux.com.iec_admin.model.ChatModel;
import developersjeux.com.iec_admin.model.UsersModel;
import developersjeux.com.iec_admin.universal.APIs;
import developersjeux.com.iec_admin.universal.AppController;
import developersjeux.com.iec_admin.universal.HelperClass;
import developersjeux.com.iec_admin.universal.IecPreferences;
import developersjeux.com.iec_admin.universal.VolleyMultipartRequest;

public class ChatActivity extends AppCompatActivity implements View.OnClickListener {

    private Context context;
    private Uri resultUri;
    private ImageView iv_Back, iv_Menu, iv_Attach, iv_Send;
    private CircleImageView civ_Picture;
    private RecyclerView rclView_Chat;
    private EditText et_Message;
    private UsersModel model;
    private IecPreferences preferences;
    private ProgressBar progressBar_Image;
    private TextView tv_Name;
    private ArrayList<ChatModel> previousChat = new ArrayList<>();
    private ChatAdapter chatAdapter;
    private Handler handler;
    private String TAG = "VOLLEY_LIBRARY";
    private boolean isFirstTime = true;
    private ArrayList<String> msgIds = new ArrayList<>();
    private String filePath;
    private Runnable r;
    private Animation scale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_new);

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorWhite));
        }

        init();
        linkViews();
        setupRecyclerView();
        setClickListener();
        setData();
        handlerRequestsForGettingChats();
    }

    private void handlerRequestsForGettingChats() {
        try {
            handler = new Handler();
            r = new Runnable() {
                public void run() {
                    handler.postDelayed(this, 1000);
                    getAllChats();
                }
            };

            handler.postDelayed(r, 1000);
        } catch (Exception e) {
            handler.removeCallbacks(r);
        }

    }

    private void setData() {
        tv_Name.setText(model.getName());
        Glide.with(context).load(model.getImage()).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {

                civ_Picture.setImageResource(R.drawable.ic_person_black_24dp);
                progressBar_Image.setVisibility(View.GONE);
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                progressBar_Image.setVisibility(View.GONE);
                return false;
            }
        }).into(civ_Picture);
    }

    private void init() {
        context = ChatActivity.this;
        Intent intent = getIntent();
        model = (UsersModel) intent.getSerializableExtra("DATA");
        preferences = new IecPreferences(ChatActivity.this);
        scale = AnimationUtils.loadAnimation(ChatActivity.this, R.anim.button_click_anim);
    }

    private void linkViews() {
        iv_Back = findViewById(R.id.iv_Back);
        civ_Picture = findViewById(R.id.civ_Picture);
        iv_Menu = findViewById(R.id.iv_Menu);
        rclView_Chat = findViewById(R.id.rclView_Chat);
        et_Message = findViewById(R.id.et_Message);
        iv_Attach = findViewById(R.id.iv_Attach);
        iv_Send = findViewById(R.id.iv_Send);
        tv_Name = findViewById(R.id.tv_Name);
        progressBar_Image = findViewById(R.id.progressBar_Image);
    }

    private void setClickListener() {
        iv_Back.setOnClickListener(this);
        iv_Menu.setOnClickListener(this);
        iv_Attach.setOnClickListener(this);
        iv_Send.setOnClickListener(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                resultUri = result.getUri();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                    uploadBitmap(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    private void uploadBitmap(final Bitmap bitmap) {
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST,
                APIs.CHAT + preferences.getUserId() + "/" + model.getId() + APIs.API_KEY,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        if (response.data.length > 0) {
                            Log.v("CheckMessageSend", "Response: " + response);
                            try {
                                JSONObject object = new JSONObject(new String(response.data));
                                String message = object.getString("message");
                                sendNotification(message);
                                String nameOfReciver = object.getString("nameOfReciver");
                                String time = object.getString("time");
                                String senderId = object.getString("senderId");
                                ChatModel model = new ChatModel(message, nameOfReciver, time, senderId);
                                if (previousChat.size() == 0) {
                                    previousChat.add(model);
                                    chatAdapter.setData(previousChat);
                                    rclView_Chat.scrollToPosition(chatAdapter.getItemCount() - 1);
                                } else {
                                    if (!previousChat.get(previousChat.size() - 1).getTime().equals(model.getTime()))
                                        previousChat.add(model);
                                    chatAdapter.notifyDataSetChanged();
                                    rclView_Chat.scrollToPosition(chatAdapter.getItemCount() - 1);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("nameOfReciver", model.getName());
                params.put("time", String.valueOf(HelperClass.getTime()));
                params.put("senderId", preferences.getUserId());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();
                params.put("message", new DataPart(imagename + ".png", getFileDataFromDrawable(bitmap)));
                return params;
            }
        };
        Volley.newRequestQueue(this).add(volleyMultipartRequest);
    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_Back:
                iv_Back.startAnimation(scale);
                finish();
                break;
            case R.id.iv_Attach:
                CropImage.activity()
                        .start(ChatActivity.this);
                break;
            case R.id.iv_Menu:
                finish();
                break;
            case R.id.iv_Send:
                iv_Send.startAnimation(scale);
                sendMessage();
                break;
        }
    }

    private void sendMessage() {
        String message = et_Message.getText().toString().trim();
        clearEditText();
        if (!message.equals("")) {
            sendMessageToServer(message);
        }
    }

    private void getAllChats() {
        Log.v("GetAllChats", "getAllChats ");
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                APIs.CHAT + preferences.getUserId() + "/" + model.getId() + APIs.API_KEY,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.v("GetAllChats", "Response: " + response.toString());
                        ArrayList<ChatModel> newChat = new ArrayList<>();
                        if (response.length() > 0) {
                            Iterator<String> iterator = response.keys();
                            while (iterator.hasNext()) {
                                String messageId = iterator.next();
                                Log.v("GetAllChats", "Iterator ID: " + messageId);
                                try {
                                    JSONObject object1 = response.getJSONObject(messageId);
                                    String message = object1.getString("message");
                                    Log.v("GetAllChats", "Message" + message);
                                    String nameOfReciver = object1.getString("nameOfReciver");
                                    String time = object1.getString("time");
                                    String senderId = object1.getString("senderId");
                                    ChatModel model = new ChatModel(message, nameOfReciver, time, senderId);
                                    model.setMessageId(messageId);
                                    newChat.add(model);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Log.v("GetAllChats", "Exception: " + e.getMessage());
                                }

                            }

                            if (previousChat.size() == 0 && newChat.size() != 0) {
                                previousChat = newChat;
                                chatAdapter.setData(previousChat);
                                rclView_Chat.setAdapter(chatAdapter);
                                rclView_Chat.scrollToPosition(chatAdapter.getItemCount() - 1);
                            } else if (newChat.size() > previousChat.size()) {
                                int insertIndex = previousChat.size();
                                for (int i = previousChat.size(); i < newChat.size(); i++) {
                                    previousChat.add(newChat.get(i));
                                    chatAdapter.notifyItemRangeInserted(insertIndex, previousChat.size());
                                    rclView_Chat.setAdapter(chatAdapter);
                                    chatAdapter.notifyItemRangeInserted(insertIndex, previousChat.size());
                                    rclView_Chat.scrollToPosition(chatAdapter.getItemCount() - 1);
                                }
                            }
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Log.v("GetAllChats", "Error: " + error.getMessage());
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjReq, TAG);
    }

    private void setupRecyclerView() {
        chatAdapter = new ChatAdapter(this);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mLayoutManager.setStackFromEnd(true);
        rclView_Chat.setLayoutManager(mLayoutManager);
        rclView_Chat.setItemAnimator(new DefaultItemAnimator());
        rclView_Chat.setHasFixedSize(true);
        rclView_Chat.setAdapter(chatAdapter);
    }

    private void sendMessageToServer(final String message) {
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                APIs.CHAT + preferences.getUserId() + "/" + model.getId() + APIs.API_KEY,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (!response.isEmpty()) {
                            sendNotification(message);
                            Log.v("CheckMessageSend", "Response: " + response);
                            try {
                                JSONObject object = new JSONObject(response);
                                String message = object.getString("message");
                                String nameOfReciver = object.getString("nameOfReciver");
                                String time = object.getString("time");
                                String senderId = object.getString("senderId");
                                ChatModel model = new ChatModel(message, nameOfReciver, time, senderId);
                                if (previousChat.size() == 0) {
                                    previousChat.add(model);
                                    chatAdapter.setData(previousChat);
                                    rclView_Chat.scrollToPosition(chatAdapter.getItemCount() - 1);
                                } else {
                                    if (!previousChat.get(previousChat.size() - 1).getTime().equals(model.getTime()))
                                        previousChat.add(model);
                                    chatAdapter.notifyDataSetChanged();
                                    rclView_Chat.scrollToPosition(chatAdapter.getItemCount() - 1);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d("volley", "Error: " + error.getMessage());
                        error.printStackTrace();
                        Log.v("CheckMessageSend", "Error: " + error.getMessage());
                        Toast.makeText(context, "Message sending Failed", Toast.LENGTH_SHORT).show();
                    }
                }) {

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("message", message);
                params.put("nameOfReciver", model.getName());
                params.put("time", String.valueOf(HelperClass.getTime()));
                params.put("senderId", preferences.getUserId());
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private void clearEditText() {
        et_Message.setText("");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            handler.removeCallbacks(r);
        } catch (Exception e) {

        }
    }

    private void sendNotification(final String message) {
        Log.v("CheckNotifySend", "API: " + APIs.SEND_NOTIFICATION + "/" + model.getId() + "/" + preferences.getUserId() + APIs.API_KEY);
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                APIs.SEND_NOTIFICATION + model.getId() + "/" + preferences.getUserId() + APIs.API_KEY,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (!response.isEmpty()) {
                            Log.v("CheckNotifySend", "Response: " + response);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d("volley", "Error: " + error.getMessage());
                        error.printStackTrace();
                        Log.v("CheckNotifySend", "Error: " + error.getMessage());
                        Toast.makeText(context, "Notification sending Failed", Toast.LENGTH_SHORT).show();
                    }
                }) {

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("nameOfReciver", model.getName());
                params.put("time", String.valueOf(HelperClass.getTime()));
                params.put("senderId", preferences.getUserId());
                params.put("message", message);
                params.put("senderName", preferences.getName());
                params.put("userImage", preferences.getUserImage());
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest);
    }
}
