package developersjeux.com.iec_admin.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import developersjeux.com.iec_admin.R;
import developersjeux.com.iec_admin.universal.IecPreferences;

public class SplashActivity extends AppCompatActivity {

    private ImageView iv_Splash;
    private IecPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        preferences = new IecPreferences(SplashActivity.this);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        iv_Splash = findViewById(R.id.iv_Splash);
        Animation myAnim = AnimationUtils.loadAnimation(this, R.anim.mytransition);
        iv_Splash.startAnimation(myAnim);
        openLoginActivity();
    }

    private void openLoginActivity() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (preferences.isLogin()) {
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                    finish();
                } else {
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    finish();
                }
            }
        }, 2500);
    }
}
