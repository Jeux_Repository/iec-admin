package developersjeux.com.iec_admin.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.appcompat.widget.SearchView;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

import developersjeux.com.iec_admin.R;
import developersjeux.com.iec_admin.adapter.UsersAdapter;
import developersjeux.com.iec_admin.fragment.InboxFragment;
import developersjeux.com.iec_admin.model.UsersModel;
import developersjeux.com.iec_admin.service.NotificationService;
import developersjeux.com.iec_admin.universal.APIs;
import developersjeux.com.iec_admin.universal.AppController;
import developersjeux.com.iec_admin.universal.IecPreferences;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static String currentUserName = "";
    private TextView tv_Title, tv_SeeMore;
    private ProgressBar progressBar;
    private SearchView searchView;
    private ImageView iv_More;
    private RecyclerView rclView_Users, rclView_Groups;
    private LinearLayout lnr_SeeMore;
    private CardView cv_AllChat;
    private Fragment fragment;
    private FragmentTransaction fragmentTransaction;
    private String TAG = "VOLLEY_LIBRARY";
    private IecPreferences preferences;
    //Instances for Users
    public static ArrayList<UsersModel> usersList;
    private UsersAdapter usersAdapter;
    private Context context;
    private ArrayList<String> userIds = new ArrayList<>();
    int counter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorWhite));
        }

        linkViews();
        setClickListener();
        init();
        getALlUsers();
        enableSearching();
//        getNotifications();
    }

    @Override
    protected void onResume() {
        super.onResume();
        startService();
    }

    private void linkViews() {
        iv_More = findViewById(R.id.iv_More);
        rclView_Users = findViewById(R.id.rclView_Users);
        tv_SeeMore = findViewById(R.id.tv_SeeMore);
        cv_AllChat = findViewById(R.id.cv_AllChat);
        progressBar = findViewById(R.id.progressBar);
        lnr_SeeMore = findViewById(R.id.lnr_SeeMore);
        tv_Title = findViewById(R.id.tv_Title);
        searchView = findViewById(R.id.searchView);
    }

    private void setClickListener() {
        iv_More.setOnClickListener(this);
        tv_SeeMore.setOnClickListener(this);
        cv_AllChat.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_More:
                showMoreMenu(iv_More);
                break;
            case R.id.tv_SeeMore:
                break;
            case R.id.cv_AllChat:
                Log.v("CheckAllUsers", "AllUsersSize: " + usersList.size());
                startActivity(new Intent(this, ChatWithAllActivity.class)
                        .putExtra("ALL_USERS", usersList)
                );
                break;
        }
    }

    private void init() {
        context = MainActivity.this;
        preferences = new IecPreferences(MainActivity.this);
    }

    private void enableSearching() {
        // listening to search query text change
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                Log.v("onQueryTextSubmit", query);
                usersAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                Log.v("onQueryTextChange", query);
                // filter recycler view when text is changed
                usersAdapter.getFilter().filter(query);
                return false;
            }
        });
    }

    private void setUpUsersList() {
        rclView_Users = findViewById(R.id.rclView_Users);
        if (usersList.size() > 5) {
            usersAdapter = new UsersAdapter(usersList, context, 5);
        } else {
            lnr_SeeMore.setVisibility(View.GONE);
            usersAdapter = new UsersAdapter(usersList, context, usersList.size());
        }
        rclView_Users.setAdapter(usersAdapter);
        if (usersList.size() > 5) {
            rclView_Users.setLayoutManager(new GridLayoutManager(context, 5));
        } else {
            rclView_Users.setLayoutManager(new GridLayoutManager(context, usersList.size()));
        }
    }

    private void getALlUsers() {
        enableProgress();
        usersList = new ArrayList<>();
        usersList.clear();
        Log.v("GetALlUsers", "API: " + APIs.USERS + APIs.API_KEY);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                APIs.USERS + APIs.API_KEY, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.v("GetALlUsers", "Response: " + response.toString());
                        if (response.length() > 0) {
                            Iterator<String> iterator = response.keys();
                            while (iterator.hasNext()) {
                                String id = iterator.next();
                                if (!id.equals(preferences.getUserId())) {
                                    try {
                                        userIds.add(id);
                                        JSONObject object = response.getJSONObject(id);
                                        String email = object.getString("email");
                                        String name = object.getString("displayName");
                                        String imageLink = object.getString("Image");
                                        UsersModel model = new UsersModel(id, imageLink, name, email);
                                        usersList.add(model);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        disableProgress();
                                    }
                                }
                            }
                            Log.v("GetALlUsers", "UserList Size " + usersList.size());
                            disableProgress();
                            if (usersList.size() != 0) {
                                setUpUsersList();
                            }
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Log.v("GetALlUsers", "Error: " + error.getMessage());
                disableProgress();
            }
        });

        AppController.getInstance().addToRequestQueue(jsonObjReq, TAG);
    }

    private void showMoreMenu(View view) {
        PopupMenu popup = new PopupMenu(MainActivity.this, view);
        popup.getMenuInflater().inflate(R.menu.more_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.logout:
                        preferences.setUserData(false, "", "", "", "", "");
                        startActivity(new Intent(MainActivity.this, LoginActivity.class));
                        finish();
                        break;
                    case R.id.inbox:
                        showInboxFragment();
                        break;
                }
                return true;
            }
        });
        popup.show();
    }

    private void enableProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    private void disableProgress() {
        progressBar.setVisibility(View.GONE);
    }

    private void startService() {
        Intent intent = new Intent(MainActivity.this, NotificationService.class);
        startService(intent);
    }

    public void showInboxFragment() {
//        Bundle bundle = new Bundle();
//        bundle.putParcelableArrayList("DATA", usersList);
        fragment = new InboxFragment();
//        fragment.setArguments(bundle);
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.rl_Container, fragment);
        fragmentTransaction.commit();
    }

}
